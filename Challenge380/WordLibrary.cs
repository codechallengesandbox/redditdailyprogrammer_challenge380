﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Challenge380
{
    class WordLibrary
    {
        readonly List<string> words = new List<string>();

        public WordLibrary()
        {
        }

        public void Load(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new InvalidOperationException($"File not found at path: {filePath}");
            }

            using var sr = new StreamReader(filePath);
            while (!sr.EndOfStream)
            {
                words.Add(sr.ReadLine());
            }
        }

        public List<string> GetWordList()
        {
            return words;
        }
    }
}
