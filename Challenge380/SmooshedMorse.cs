﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Challenge380
{
    class SmooshedMorse
    {
        const string morseValues = ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --..";
        readonly Dictionary<char, string> encodings;

        public SmooshedMorse()
        {
            encodings = morseValues
                .Split(new char[] { ' ' }) // Space delimited
                .Select((value, index) => new { value, index }) // Convert to Key Value Pair on array index
                .ToDictionary(kvp => (char)(kvp.index + 'a'), kvp => kvp.value); // Convert to Dictionary on char index
        }

        public string Encode(string input)
        {
            // Strip out illegal characters
            input = StripIllegalCharacters(input);

            return string.Join("", input.SelectMany(x => encodings[x]));
        }

        private string StripIllegalCharacters(string input)
        {
            return new string(input.Where(c => c >= 'a' && c <= 'z').ToArray());
        }
    }
}
