﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Challenge380
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            // Setup
            var rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var library = new WordLibrary();
            library.Load(Path.Combine(rootDirectory, "Assets/enable1.txt"));
            var words = library.GetWordList();

            var smorse = new SmooshedMorse();

            var encodedWords = words.Select(x => new { word = x, encoding = smorse.Encode(x) });

            // The sequence -...-....-.--. is the code for four different words (needing, nervate, niding, tiling). Find the only sequence that's the code for 13 different words.
            Console.WriteLine("Sequences that represent 13 different words");
            var list1 = encodedWords.GroupBy(x => x.encoding).Where(x => x.Count() == 13);
            foreach (var sequence in list1)
            {
                Console.WriteLine($"Sequence: {sequence.Key}");
                foreach (var word in sequence.ToList())
                {
                    Console.WriteLine($"Word: {word.word}, Encoding: {word.encoding}");
                }
            }

            // autotomous encodes to .-..--------------..-..., which has 14 dashes in a row. Find the only word that has 15 dashes in a row.
            Console.WriteLine();
            Console.WriteLine("Encoded words with 15 dashes");
            var list2 = encodedWords.Where(x => x.encoding.Contains(new string('-', 15)));
            foreach (var word in list2)
            {
                Console.WriteLine($"Word: {word.word}, Encoding: {word.encoding}");
            }

            // Call a word perfectly balanced if its code has the same number of dots as dashes. counterdemonstrations is one of two 21-letter words that's perfectly balanced. Find the other one.
            Console.WriteLine();
            Console.WriteLine("21 letter words that have balanced encodings");
            var list3 = encodedWords
                .Where(x => x.word.Length == 21 && x.word != "counterdemonstrations")
                .Select(x => new { x.word, x.encoding, dots = x.encoding.Where(x => x == '.').Count(), dashes = x.encoding.Where(x => x == '-').Count() })
                .Where(x => x.dots == x.dashes);
            foreach (var word in list3)
            {
                Console.WriteLine($"Word: {word.word}, Encoding: {word.encoding}, Dots: {word.dots}, Dashes: {word.dashes}");
            }

            // protectorate is 12 letters long and encodes to .--..-.----.-.-.----.-..--., which is a palindrome (i.e. the string is the same when reversed). Find the only 13-letter word that encodes to a palindrome.
            Console.WriteLine();
            Console.WriteLine("13 letter words that encode to a palindrome");
            var list4 = encodedWords
                .Where(x => x.word.Length == 13)
                .Select(x => new { x.word, x.encoding, reversed = new string(x.encoding.Reverse().ToArray()) })
                .Where(x => x.encoding == x.reversed);
            foreach (var word in list4)
            {
                Console.WriteLine($"Word: {word.word}, Encoding: {word.encoding}, Reversed: {word.reversed}");
            }

            // --.---.---.-- is one of five 13-character sequences that does not appear in the encoding of any word. Find the other four.
            Console.WriteLine();
            Console.WriteLine("13 character sequences that are not contained in the encoding of any word");
            var list5 = encodedWords
                .Where(x => x.encoding.Length >= 13)
                .Distinct()
                .ToArray();

            for (var i = 0; i <= Convert.ToInt32(new string('1', 13), 2); i++)
            {
                var permutation = Convert.ToString(i, 2).Replace('0', '.').Replace('1', '-');

                if (!list5.Where(x => x.encoding.Contains(permutation)).Any())
                {
                    Console.WriteLine($"Sequence: {Convert.ToString(i, 2).Replace('0', '.').Replace('1', '-')}");
                }
            }

            stopwatch.Stop();
            Console.WriteLine();
            Console.WriteLine($"Completed in {stopwatch.ElapsedMilliseconds} ms");
            Console.ReadLine();
        }
    }
}
